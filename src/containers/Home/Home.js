import React from 'react';
import PropTypes from 'prop-types';
import { AppBar } from 'src/utils/material-ui/index.js'
// import PullRefresh from 'react-pullrefresh'


class HomeView extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    fab: PropTypes.element,
    iconElementRight: PropTypes.element,
  };

  static contextTypes = {
    drawer: PropTypes.object,
    setFab: PropTypes.func,
  };

  componentDidMount() {
    this.context.setFab(this.props.fab);
  }

  componentWillUnmount() {
    this.context.setFab(null);
  }

  render = () => {
    const conditional = {};
    if (this.props.iconElementRight) {
      conditional.iconElementRight = this.props.iconElementRight
    }
    if(this.props.backgroundColor) {
      conditional.style = {
        backgroundColor: this.props.backgroundColor
      }
    }

    return (
      <div style={{ height: '100%', overflow: 'hidden'}}>
        <AppBar
          title={this.props.title}
          onLeftIconButtonClick={this.toogleDrawer}
          { ...conditional }
        />
        <div style={{ position: 'relative', minHeight: '100%' }}>
          {this.props.children}
        </div>
      </div>
    );
  }

  toogleDrawer = () => {
    this.context.drawer.getWrappedInstance().toogle();
  }


}


export default HomeView;
