import { Types as TypesAuth } from 'src/actions/auth'
import createReducer from '../utils/createReducer'
import avatarFa from '../containers/Home/components/Drawer/avatar.jpg'


const initialState = {
  name: "Fátima Begonia",
  email: "f.begonia@gmail.com",
  id: 1,
  avatar: avatarFa,
};

export default createReducer(initialState, {
  [TypesAuth.AUTH_LOGIN_USER_SUCCESS]: (state, action) => ({
    ...state,
    ...action.payload.user,
  }),

  [TypesAuth.AUTH_LOGOUT_USER]: (state, action) => ({
    ...initialState
  }),
})

