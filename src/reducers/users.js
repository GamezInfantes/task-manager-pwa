import createReducer from '../utils/createReducer'
import { Types as TypesAuth } from '../actions/auth';


const initialState = {
  2: {
    id: 2,
    name: "Cristian",
    email: "c.gamezinfantes@gmail.com",
    avatar: "https://lh3.googleusercontent.com/-p0bQAJrQWe4/AAAAAAAAAAI/AAAAAAAAAAA/AA6ZPT630Ia-r5CevSGz7DQM0WaiG5aSAg/s32-c-mo/photo.jpg",
    sharedTasks: []
  }
};

export default createReducer(initialState, {
  [TypesAuth.AUTH_LOGOUT_USER]: (state, action) => ({
    ...initialState
  }),
})


export const getUsers = (state) => {
  return Object.values(state.users)
}


