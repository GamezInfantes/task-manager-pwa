import React from 'react';
import { Route, Switch } from 'react-router';
import PrivateRoute from './utils/requireAuthentication';
import EditTask from './containers/EditTask'

const List = asyncComponent(() => import('./containers/List/List'))
const AddList = asyncComponent(() => import('./containers/AddList/AddList'))
const Login = asyncComponent(() => import('./containers/Login/Login'))
const Settings = asyncComponent(() => import('./containers/Settings/Settings'))
const NotFound = asyncComponent(() => import('./containers/NotFound'))
const TasksWeek = asyncComponent(() => import('./containers/TasksWeek/TasksWeek'))
const AddTask = asyncComponent(() => import('./containers/AddTask/AddTask'))
const TasksToday = asyncComponent(() => import('./containers/TasksToday/TasksToday'))
// const EditTask = asyncComponent(() => import('./containers/EditTask'))
const EditList = asyncComponent(() => import('./containers/EditList/EditList'))
const TasksMonth = asyncComponent(() => import('./containers/TasksMonth/TasksMonth'))
const UserTaks = asyncComponent(() => import('./containers/UserTaks/UserTaks'))


function asyncComponent(importComponent) {
  class AsyncComponent extends React.Component {
    state = {
      component: null
    };

    componentDidMount() {
      importComponent().then(res => {
        const { default: component } = res
        this.setState({
          component: component
        });
      });
    }

    render() {
      const C = this.state.component;
      return C ? <C {...this.props} /> : null;
    }
  }

  return AsyncComponent;
}


export default (
  <Switch>
    {/* <Route exact path="*" component={containers.Settings} /> */}
    <PrivateRoute exact path="/" component={TasksToday} />
    <PrivateRoute exact path="/week" component={TasksWeek} />
    <PrivateRoute path="/add" component={AddTask} />
    <PrivateRoute path="/user/:id" component={UserTaks} />
    <PrivateRoute path="/task/:id" component={EditTask} />
    <PrivateRoute path="/list/:id/edit" component={EditList} />
    <PrivateRoute path="/list/:id" component={List} />
    <PrivateRoute path="/month" component={TasksMonth} />
    <PrivateRoute path="/add-list" component={AddList} />
    <Route path="/login" component={Login} />
    <PrivateRoute path="/settings" component={Settings} />
    <Route path="*" component={NotFound} />
  </Switch>
);
