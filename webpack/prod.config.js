const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ClosurePlugin = require('closure-webpack-plugin');


module.exports = {
  // devtool: 'source-map', // No need for dev tool in production

  module: {
    rules: [{
      test: /\.css$/,
      use: ExtractTextPlugin.extract([
        {
          loader: 'css-loader',
          options: { importLoaders: 1 },
        },
        'postcss-loader']
      )
    }, {
      test: /\.scss$|\.sass$/,

      use: ExtractTextPlugin.extract([
        {
          loader: 'css-loader',
          options: { importLoaders: 1 },
        },
        'postcss-loader',
        {
          loader: 'sass-loader',
          options: {
            data: `@import "${__dirname}/../src/static/styles/config/_variables.sass";`
          }
        }]
      )
    }],
  },

  plugins: [
    new ExtractTextPlugin('styles/[name]-[hash].css'),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: !false
    }),
    // new ClosurePlugin({ mode: 'STANDARD' }, {
    //   // formatting: 'PRETTY_PRINT'
    //   debug: true
    // })
  ]
};
